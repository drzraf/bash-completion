# bash completion for cryptmount                           -*- shell-script -*-

_cryptmount_target()
{
    cryptmount -l|awk '{print $1}'
}

_cryptmount()
{
    local cur prev words cword split
    _init_completion -s || return

    case $prev in
        --change-password|-c|--mount|-m|--unmount|-u|--prepare| \
	    --release|--reuse-key|-e|--list|-l)
            COMPREPLY=( $( compgen -W '$( _cryptmount_target )' -- "$cur" ) )
            return 0
            ;;
        --config-fd|--passwd-fd)
            COMPREPLY=( $( compgen -W '1 2' -- "$cur" ) )
            return 0
            ;;
	# depends on /proc/crypto but can't know correct values
	# without parsing "keyformat" from /etc/cryptmount/cmtab
	--generate-key)
            COMPREPLY=( $( compgen -W '16 32 64 128 256' -- "$cur" ) )
            return 0
            ;;
	--key-managers|-k|--version|--all)
	    return 0
	    ;;
    esac

    local arg pprev
    _get_first_arg
    if [[ -z $arg ]]; then
        if [[ "$cur" == -* ]]; then
            COMPREPLY=( $( compgen -W '$( _parse_help "$1" )' -- "$cur" ) )
	else
            COMPREPLY=( $( compgen -W '$( _cryptmount_target ) -' -- "$cur" ) )
        fi

    # Handle options that require two arguments.
    elif [[ COMP_CWORD -gt 1 ]]; then
        pprev="${COMP_WORDS[COMP_CWORD-2]}"
        case "$pprev" in
            --reuse-key)
                COMPREPLY=( $( compgen -W '$( _cryptmount_target )' -X "$prev" -- "$cur" ) )
                ;;
            --generate-key)
                COMPREPLY=( $( compgen -W '$( _cryptmount_target )' -- "$cur" ) )
                ;;
        esac
    fi

    return 0
} &&
complete -F _cryptmount cryptmount

# ex: ts=4 sw=4 et filetype=sh
